# This is the README of the community repository of Androguard

By default you must have the androguard directory and to get all samples:

hg clone https://anthony.desnos@code.google.com/p/androguard/

git clone https://bitbucket.org/androguard/community.git


androclassloader.py: analyse a directory of android apps and display app which use use the classloader

androperm.py:  analyse a directory of android apps and display permission of each APK

androsms.py: analyse a directory of android apps and filter only APK with SEND_SMS permission, display source code of each method where the SEND_SMS permission is used

androsparsepacked.py: analyse a directory of android apps and display (sparse/packed)-switch instructions

filescan.py is a script that analyses the files contained inside an apk. It is able to identify textual files (checked for URLs, phone numbers, shell commands), compressed archives (recursively analysed), dex code and ELF files (checked against the hash codes of known malware), and produce a final risk score.

